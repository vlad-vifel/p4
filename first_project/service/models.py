from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=120, blank=False)
    describtion = models.CharField(max_length=620, blank=False)
    price = models.IntegerField(default=0)

    def __str__(self):
        return self.title

class Former(models.Model):
    CHOICES = (('1','Сайт на Тильде'),('2','Веб-дизайн'))

    name = models.CharField(max_length=60)
    email = models.EmailField()
    service = models.CharField(max_length=1, choices=CHOICES)
    commentary = models.CharField(max_length=600)

    def __str__(self):
        return self.name + " " + self.email
