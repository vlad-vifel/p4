from django.shortcuts import render
from service.models import Post
from service.models import Former
from service.forms import ContactForm


def index(request):
    context = {}

    all_posts = Post.objects.all()
    context['posts'] = all_posts
    
    if request.method == 'POST':
        contact_form = ContactForm(request.POST)
        if contact_form.is_valid(): 
            former = Former(
                name = contact_form.cleaned_data['name'],
                email = contact_form.cleaned_data['email'],
                service = contact_form.cleaned_data['service'],
                commentary = contact_form.cleaned_data['commentary']
            )
            former.save()
            context['success'] = '1'
    else:
        contact_form = ContactForm()
    context['form'] = contact_form
    
    return render(request, 'index.html', context = context)

