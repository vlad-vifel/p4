from django import forms
from service.models import Former


class ContactForm(forms.ModelForm):
    class Meta:
        model = Former

        fields = ['name', 'email', 'service', 'commentary']

        widgets = {
            'name': forms.TextInput(
                attrs={'placeholder': 'Имя',
                       'class': 'p block-application__field'}
            ),

            'email': forms.EmailInput(
                attrs={'placeholder': 'Почта',
                       'class': 'p block-application__field'}
            ),

            'service': forms.Select(
                attrs={'class': 'p block-application__select'}
            ),

            'commentary': forms.Textarea(
                attrs={'placeholder': 'Комментарий', 'rows': 4,
                       'class': 'p block-application__field'}
            )
        }
