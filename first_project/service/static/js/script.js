window.addEventListener('DOMContentLoaded', () => { 

    document.querySelector('.works__nav').addEventListener('click', (event) => {
        if(event.target.classList.contains('nav-works__button')) {

            document.querySelector('.nav-works__button--active').classList.remove('nav-works__button--active')

            event.target.classList.add('nav-works__button--active')

            const filterValue = event.target.getAttribute('data-filter')


            document.querySelectorAll('.gallery__photo').forEach((photo) => {


                if (filterValue === 'all') {
                    photo.classList.remove('gallery__photo--shown')
                    photo.classList.remove('gallery__photo--hidden')

                    if(photo.classList.contains('gallery__photo_small')) {
                        photo.classList.remove('gallery__photo_big')
                    }

                } else if (photo.getAttribute('alt') === filterValue) {
                    photo.classList.remove('gallery__photo--hidden')
                    photo.classList.add('gallery__photo--shown')
                    
                    console.log('!!!')

                    if(photo.classList.contains('gallery__photo_small')) {
                        console.log('!!!!!')
                        photo.classList.add('gallery__photo_big')
                    }

                } else {
                    photo.classList.remove('gallery__photo--shown')
                    photo.classList.add('gallery__photo--hidden')

                    if(photo.classList.contains('gallery__photo_small')) {
                        photo.classList.remove('gallery__photo_big')
                    }
                }



            })

            

        }
    })

    document.querySelector('.cost').addEventListener('click', (event) => {

        const workType = document.querySelector('#select__work-type')
        const workSize = document.querySelector('#select__work-size')
        const checkbox = document.querySelector('.checkbox')
        const button = document.querySelector('.options-cost__button')

        const blockResult = document.querySelector('.cost__result')
        const cost = document.querySelector('.result-cost__cost')
        const duration = document.querySelector('.result-cost__duration')


        const costType = new Map()
        costType.set('s2', 20000).set('s3', 15000)
        const costSize = new Map()
        costSize.set('s2', 4).set('s3', 5)

        const durationType = new Map()
        durationType.set('s2', 4).set('s3', 3)
        const durationSize = new Map()
        durationSize.set('s2', 3).set('s3', 5)

        var optionType = workType.querySelector('option:checked').getAttribute('value')
        var optionSize = workSize.querySelector('option:checked').getAttribute('value')


        if(optionType === 's1') {
            workSize.setAttribute('disabled','')
        } else {
            workSize.removeAttribute('disabled')
        }

        if(optionType === 's1' || optionSize === 's1' || !checkbox.checked) {
            button.setAttribute('disabled', '')
        } else {
            button.removeAttribute('disabled')
        }


        if(event.target.classList.contains('options-cost__button') && !button.hasAttribute('disabled')) {
            blockResult.classList.remove('cost__result--hidden')

            cost.textContent = "От " + String(costType.get(optionType)*costSize.get(optionSize)) + " ₽"

            duration.textContent = String(durationType.get(optionType)*durationSize.get(optionSize)) + " дней"
        }
    })


    document.querySelectorAll('.block-faq__container').forEach((el) => {
        el.addEventListener('click', () => {

            let container = el;
            let content = el.nextElementSibling

            if(content.classList.contains('block-faq__content--current')) {
                document.querySelectorAll('.block-faq__container').forEach((el) => { el.classList.remove('block-faq__container--current') })
                document.querySelectorAll('.block-faq__content').forEach((el) => { el.classList.remove('block-faq__content--current') })
            } else {
                document.querySelectorAll('.block-faq__container').forEach((el) => { el.classList.remove('block-faq__container--current') })
                document.querySelectorAll('.block-faq__content').forEach((el) => { el.classList.remove('block-faq__content--current') })

                content.classList.add('block-faq__content--current')
                container.classList.add('block-faq__container--current')
            }
        })

    })

})